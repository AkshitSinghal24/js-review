const arr = require("./2-arrays-logins.cjs");
const ansobj = {};
arr.map((item) => {
  if (item.email.endsWith(".org")) {
    ansobj[".org"] = (ansobj[".org"] || 0) +1;
  } else if (item.email.endsWith(".au")) {
    ansobj[".au"] = (ansobj[".au"] || 0) +1;
  } else if (item.email.endsWith(".com")) {
    ansobj[".com"] = (ansobj[".com"] || 0) +1;
  }
});

console.log(ansobj);

