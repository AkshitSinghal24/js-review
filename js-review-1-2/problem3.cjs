const arr = require("./2-arrays-logins.cjs");
const ans = arr.map((data) => {
    let ip = (data["ip_address"]);
    let tempstr = ip.split(".").map(value => Number(value));
    return tempstr
});

const sumSecond = ans.reduce(
    (accumulator, value) => accumulator + value[1],0);

const sumForth = ans.reduce(
    (accumulator, value) => accumulator + value[3],0);

console.log(sumSecond,sumForth);
