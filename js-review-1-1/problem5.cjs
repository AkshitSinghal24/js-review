const array = require("./1-arrays-jobs.cjs");
const obj = {};
array.map(item => {
    const country = item.location;
    const salary = Number(item.salary.slice(1))*10000;
    obj[country] =  (obj[country] || 0) + salary;
})
console.log(obj);